/* 
 * Descreiption: Server side javascript for Find-a-GP. Allows make up or clear data for test.
 * Author: Jiankun Rui 721393
 * Date: 15/04/2016
 */

import {Meteor} from 'meteor/meteor';

GPList = new Mongo.Collection("allgp");

// Random data generation
var states = ["ACT", "JBT", "NSW", "NT", "QLD", "SA", "TAS", "VIC", "WA"];
var languanges = ["EN", "CN", "JP", "KR", "FR", "Other"];
var suburbs = ["Preston", "Parkville", "Carlton", "Brighton",
    "Frankston", "Melbourne CBD", "North Melbourne",
    "Suburb1", "Suburb2", "Suburb3", "Suburb4"];
var names = ["Family Clinic", "Medical and Dental Centre", "Doctor To You",
    "Medical Centre", "Medical Clinic", "Children Dentist", "Family Practice",
    "Med Practice"];
var addresses = [
    {"12 Ormond Blvd, Bundoora, VIC 3083": {lat: -37.67972, lng: 145.071445}},
    {"Northland Shopping Centre, Preston Ground Floor Preston, VIC 3072": {lat: -37.738813, lng: 145.004028}},
    {"4 Dundas Street, Thornbury, VIC 3071": {lat: -37.752744, lng: 145.002508}},
    {"675A High Street, Thornbury, VIC 3071": {lat: -37.760396, lng: 144.999962}},
    {"210A Broadway, Reservoir, VIC 3073": {lat: -37.716928, lng: 145.011871}},
    {"2A Elm Street, Northcote, VIC 3070": {lat: -37.768242, lng: 144.998755}},
    {"302 Victoria Street, Brunswick, VIC 3056": {lat: -37.767171, lng: 144.963132}},
    {"318 Victoria Street, Brunswick, VIC 3056": {lat: -37.767259, lng: 144.961478}},
    {"3 Pearl Court, Cranbourne VIC 3977": {lat: -38.103418, lng: 145.264706}},
    {"163 High Street, Kew VIC 3101": {lat: -37.80663, lng: 145.03061}},
    {"415 Bourke Street, Melbourne VIC 3000": {lat: -37.814925, lng: 144.961748}},
    {"255 Bourke Street, Melbourne VIC 3000": {lat: -37.813568, lng: 144.966196}},
    {"200 Queen Street, Melbourne VIC 3000": {lat: -37.813535, lng: 144.960444}},
    {"250 Collins Street, Melbourne VIC 3000": {lat: -37.815481, lng: 144.965482}},
    {"53 Queen Street, Melbourne VIC 3000": {lat: -37.817559, lng: 144.961532}},
    {"211 La Trobe Street, Melbourne VIC 3000": {lat: -37.810916, lng: 144.962795,}},
    {"399 Elizabeth Street, Melbourne VIC 3000": {lat: -37.809334, lng: 144.960816}},
    {"108 Bourke Street, Melbourne VIC 3000": {lat: -37.81202, lng: 144.969832}},
    {"71 Collins Street, Melbourne VIC 3000": {lat: -37.814288, lng: 144.971324}},

];

function randomNum(length) {
    var num = length + 1;
    while (num > length) {
        num = Math.floor(Math.random() * length);
    }
    return num;
};


Meteor.startup(() => {
    // code to run on server at startup
    return Meteor.methods({

        removeAllGP: function () {
            return GPList.remove({});
        },

        makeupGPInfo: function () {
            ids = new Array();

            for (i = 0; i < 15; i++) {

                var addrObj = addresses[randomNum(addresses.length)];
                var key = Object.keys(addrObj)[0];
                var location = addrObj[key];
                var id = GPList.insert({
                    name: suburbs[randomNum(suburbs.length)] +
                    " " + names[randomNum(names.length)],
                    address: key,
                    location: location,
                    language: languanges[randomNum(languanges.length)],
                    suburb: suburbs[randomNum(suburbs.length)],
                    state: states[randomNum(states.length)]
                });
                ids.push(id);
            }
            return ids;
        },
    });
});
