/**
 * Created by ruijiankun on 17/05/2016.
 */

Meteor.startup(function () {
    GoogleMaps.load({key: "AIzaSyCuWKnVsScnGqWy-sRU0QQ1OkSCA0G_Osg"});
});

var _defMapZoom = 14;
var _currentLocation;
var _markers;

Template.mapViewTemplate.events({
    'click #recenter': function (e) {
        if (GoogleMaps.loaded() && _currentLocation) {
            GoogleMaps.get('nearbyGPMap').instance.setCenter(_currentLocation.getPosition());
            GoogleMaps.get('nearbyGPMap').instance.setZoom(_defMapZoom);
        }
    },
});

Template.mapViewTemplate.helpers({
    mapOptions: function () {
        // Make sure the maps API has loaded
        var pos = {
            lat: -37.8136,
            lng: 144.9631
        };

        if (GoogleMaps.loaded()) {
            // Map initialization options
            pos = Geolocation.latLng();
            return {
                center: new google.maps.LatLng(pos.lat, pos.lng),
                zoom: 14
            };
        }
    }
});

Template.mapViewTemplate.onCreated(function () {
    var self = this;
    GoogleMaps.ready('nearbyGPMap', function (map) {
        // Create and move the marker when latLng changes.
        self.autorun(function () {
            var latLng = Geolocation.latLng();
            if (!latLng)
                return;
            // If the marker doesn't yet exist, create it.
            if (!_currentLocation) {
                _currentLocation = new google.maps.Marker({
                    position: new google.maps.LatLng(latLng.lat, latLng.lng),
                    map: map.instance
                });
            }
            // The marker already exists, so we'll just change its position.
            else {
                _currentLocation.setPosition(latLng);
            }

        });
    });
});