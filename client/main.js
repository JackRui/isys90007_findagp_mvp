/* 
 * Descreiption: Client side javascript for Find-a-GP. All functions for GP searching and locating.
 * Author: Jiankun Rui 721393
 * Date: 15/04/2016
 */


import {Template} from 'meteor/templating';
import {ReactiveVar} from 'meteor/reactive-var';
import {Mongo} from 'meteor/mongo';

import './main.html';

GPList = new Mongo.Collection('allgp');

var searchId = new Array();

Template.navBarTemplate.events({
    // TEST: Click link to generate make-up GP data
    'click #makeupdata': function (e) {
        e.preventDefault();
        alert("This action will insert make-up data to the server!");
        Meteor.call('makeupGPInfo')
    },

    // TEST: Click link to clear all GP data on server
    'click #cleardata': function (e) {
        e.preventDefault();
        alert("This action will clear all GP information!");
        Meteor.call('removeAllGP')
    },

    'click #searchGP': function (e) {
        e.preventDefault();
        $('#searchModal').modal('show');
    }

});

//----- search function -----
Template.searchBarTemplate.events({
    // Click search button to search
    'click #search': function (e) {
        e.preventDefault();
        var stateVal = document.getElementById('stateOption').value;
        var languageVal = document.getElementById('languageOption').value;
        var keywordsVal = document.getElementById('keywordText').value;

        var val = keywordsVal.replace(/\s+/g, ' ').toLowerCase();

        var $rows = $('#gptable tr');

        // To directly manipulate DOM element is not a good way, here just for  fast developing function.
        searchId = [];
        $rows.show().filter(function () {
            var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
            var matchAll = text.indexOf(val) > -1;

            var stateText = $(this).children(':eq(4)').text().toLowerCase();
            var matchState = stateText.indexOf(stateVal) > -1;

            var langText = $(this).children(':eq(1)').text().toLowerCase();
            var matchLang = langText.indexOf(languageVal) > -1;

            if ((matchAll && matchState && matchLang)) {
                //console.log($(this).children(':eq(2)')
                //.children('#addr').attr('value'));
                searchId.push($(this).children(':eq(2)')
                    .children('#addr').attr('value'))
            }
            return !(matchAll && matchState && matchLang);
        }).hide();
    },

    // When pressing ENTER, treat as search button clicked.
    'keyup #keywordText': function (e) {
        e.preventDefault();
        if (e.keyCode == 13) {
            $('#search').click();
            return;
        }
    }
});

Template.gpInfoTemplate.helpers({
    // Load GP information to table
    allgp: function () {
        return GPList.find();
    }
});

Template.gpInfoTemplate.events({
    // When clicking an address, show its location on map
    'click #addr': function (e) {
        e.preventDefault();
        //Create a url to search a specified place in google map
        var addrText = $(e.target).text() + ", Australia/";
        var urlPrefix = "https://www.google.com/maps/search/"
        var url = urlPrefix + addrText;

        window.open(url, '_blank')

    }
});
